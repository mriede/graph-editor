import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  	api: 'https://nosy.kresh.me/services.php?op=list&q=system',
  	data: {
  		systems: [],
  		currentNode: {},
  		currentEdges: {},
  	}
  },
  mutations: {
  	SET_SYSTEMS (state, array) {
  		state.data.systems = array
  	},

  	SET_CURRENT_NODE (state, node) {
  		state.data.currentNode = node
  	},

  	SET_CURRENT_EDGES (state, edges) {
  		state.data.currentEdges = edges
  	},

  	GET_SYSTEM_BY_ID (state, id) {
  		let res = state.data.systems.filter(system => system.id == id.toString())

  		console.log('Checking systems. Result: ' + res)

  		return res[0]
  	},

  	GET_NODE_BY_ID (state, id) {

  	}
  },
  actions: {
  	fetchAllSystems (context) {
  		axios.get('https://nosy.kresh.me/services.php?op=list&q=system')
			  .then(response => {
			    console.log(response)
			    context.commit('SET_SYSTEMS', response.data)
			  })

			  .catch(error => {
			    // handle error
			    console.log(error)
			  })
  	},

  	fetchNode (context, id) {
  		axios.get('https://nosy.kresh.me/services.php?op=list&q=node&node=' + id)
      .then(response => {
        console.log(response)
        context.commit('SET_CURRENT_NODE', response.data[0])
      })

      .catch(error => {
        // handle error
        console.log(error)
      })
    },
  	fetchEdges (context, id) {
  		axios.get('https://nosy.kresh.me/services.php?op=list&q=edge&node=' + id)
      .then(response => {
        console.log(response)
        context.commit('SET_CURRENT_EDGES', response.data)
      })

      .catch(error => {
        // handle error
        console.log(error)
      })
    },
  	
  },
  getters: {
  	systems: state => {
  		return state.data.systems
  	},

  	currentNode: state => {
  		return state.data.currentNode
  	},

  	currentEdges: state => {
  		return state.data.currentEdges
  	},
  },
})

