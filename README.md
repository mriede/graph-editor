# Graph Editor

This is the front-end code for [Nosy's Graph Editor](https://nosy.kresh.me).

## Project setup

Run the following command after cloning this repository, to install the necessary NPM packages.

```bash
$ yarn # or 'npm install'
```

After the process has been completed, you can launch a development server or build for production

### Compiles and hot-reloads for development
```bash
# Launch a hot-reloading development server
$ yarn serve # or 'npm run serve'

# Build a production bundle. The result of this command can be found in dist/
$ yarn build # or 'npm run build'

# Run tests
$ yarn test:e2e # or 'npm run test:e2e'
$ yarn test:unit # or 'npm run test:unit'
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
